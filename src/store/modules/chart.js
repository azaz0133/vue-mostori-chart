import Axios from "axios";
import moment from 'moment'
const API = "http://localhost:3030/tempal"
let i = 0;
const state = {
    isLoading: false,
    data: [],
    minTemp: 0,
    maxTemp: 0,
    sec: 15
}

const getters = {
    getFetchData: state => state.data,
    getMin: state => state.minTemp,
    getMax: state => state.maxTemp,
    getSec: state => state.sec
}

const actions = {
    fetchData: async ({ commit }, payload) => {
        // const { data: { data } } = await Axios.get(API)
        let data = MOCK
        setTimeout(() => {

            commit('DISPATH_FETCH_DATA', data)
        }, 1000)
    }
}

const mutations = {

    DISPATH_FETCH_DATA: (state, payload) => {
        const info = payload.map(data => ({
            date: moment(data.createdAt).format("DD/MM/YY:hh/mm/ss"),
            battery: data.battery,
            temp: parseInt(data.temp) + (++i)
        })
        )
        state.data = info
    },

    SET_MINTEMP: (state, payload) => {
        state.minTemp = payload
    },
    SET_MAXTEMP: (state, payload) => {
        state.maxTemp = payload
    },
    SET_SEC: (state, payload) => {
        state.sec = payload
    }

}

const MOCK = [
    {
        "_id": "5c87b9c368540d354cdedda8",
        "read": "0",
        "macAddress": "50:f1:4a:c7:b9:54",
        "temp": "16",
        "battery": "35",
        "boardId": "5c85be1cf1b14d5dc4b4bc02",
        "createdAt": "2019-03-12T13:53:07.549Z",
        "updatedAt": "2019-03-12T13:53:07.549Z",
        "__v": 0
    },
    {
        "_id": "5c87ba473e2fbd4020caa153",
        "read": "0",
        "macAddress": "50:f1:4a:c7:b9:54",
        "temp": "16",
        "battery": "35",
        "boardId": "5c85be1cf1b14d5dc4b4bc02",
        "createdAt": "2019-03-12T13:55:19.198Z",
        "updatedAt": "2019-03-12T13:55:19.198Z",
        "__v": 0
    },
    {
        "_id": "5c87c90641917f1548ef9e14",
        "read": "0",
        "macAddress": "1234",
        "temp": "16",
        "battery": "35",
        "boardId": "5c85be1cf1b14d5dc4b4bc02",
        "createdAt": "2019-03-12T14:58:14.164Z",
        "updatedAt": "2019-03-12T14:58:14.164Z",
        "__v": 0
    },
    {
        "_id": "5c88aec98c970f7e540fd724",
        "read": "0",
        "macAddress": "50:f1:4a:c7:b9:54",
        "temp": "19",
        "battery": "35",
        "boardId": "5c88a97d8c970f7e540fd723",
        "createdAt": "2019-03-13T07:18:33.682Z",
        "updatedAt": "2019-03-13T07:18:33.682Z",
        "__v": 0
    }
]


export default {
    // namespaced: true,
    state,
    getters,
    actions,
    mutations
}